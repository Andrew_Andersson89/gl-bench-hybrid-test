import {NgForm} from "@angular/forms";
import * as moment from "moment";

function GlCreateAccountCtrl($scope, $rootScope, $uibModalInstance, $filter, $state, $log) {
    "use strict";
    var glCreateAccount = this;

    glCreateAccount.statusList = [
        'Disable',
        'Pending',
        'Active'
    ];

    glCreateAccount.account = {};
    glCreateAccount.isOpenStartDatePicker = false;
    glCreateAccount.isOpenExpDatePicker = false; 


    glCreateAccount.$onInit = function () {
        glCreateAccount.account.status = 'Active'
        glCreateAccount.account.start_date = new Date();
        glCreateAccount.account.expiration_date = new Date();
    }

    glCreateAccount.openStartDatePicker = function () {
        glCreateAccount.isOpenStartDatePicker = !glCreateAccount.isOpenStartDatePicker;
    }

    glCreateAccount.openExpDatePicker = function () {
        glCreateAccount.isOpenExpDatePicker = !glCreateAccount.isOpenExpDatePicker;
    }

    glCreateAccount.onClose = function() {
        $uibModalInstance.dismiss('close modal');
    }

    glCreateAccount.submit = function(account) {
        const newAccount = account;
        newAccount.start_date = account.start_date.getTime()/1000;
        newAccount.expiration_date = account.expiration_date.getTime()/1000;
        $uibModalInstance.close(newAccount);
    }


    return glCreateAccount;
};

GlCreateAccountCtrl.$inject = ['$scope', '$rootScope', '$uibModalInstance', '$filter', '$state', '$log', 'glAccountsService'];

export default (ngModule) => {
    return ngModule.controller('GlCreateAccountCtrl', GlCreateAccountCtrl);
}
