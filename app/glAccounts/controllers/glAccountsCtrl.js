function GlAccountsCtrl($scope, $rootScope, $state, $log, $uibModal, glAccountsService, $filter) {
    let glAccounts = this;


    glAccounts.$onInit = function () {
        glAccountsService.getAccounts().$promise.then(res => {
            glAccounts.data = glAccounts.mapData(res).sort((a, b) => b.start_date - a.start_date);
        });

    }

    glAccounts.mapData = function(data) {
        return data.map(item => (item.color = $filter('glStatusMap')(item.status))&& item);
    }
    
    glAccounts.onRemove = function(id) {
        glAccountsService.deleteAccount({id}).$promise.then(_ => {
            glAccounts.data = glAccounts.data.filter(item => item.id !== id);
        });
    }

    glAccounts.onClickCreateAccount = function() {
        const uiModal = $uibModal.open({
            animation: true,
            template: require('../partials/createAccountPopUp.html'),
            controller: 'GlCreateAccountCtrl as glCreateAccount',
        });

        uiModal.result.then(async account => {
            const newAccount = await glAccountsService.createAccount(account).$promise;
            if (!newAccount.id) return;
            glAccounts.data.unshift({...newAccount});
            glAccounts.data = glAccounts.mapData(glAccounts.data);
        }, err => {})
    }

    return glAccounts;
};

GlAccountsCtrl.$inject = ['$scope', '$rootScope', '$state', '$log', '$uibModal', 'glAccountsService', '$filter'];

export default (ngModule) => {
    return ngModule.controller('GlAccountsCtrl', GlAccountsCtrl);
}
