export interface Account {
    account_name: string;
    email: string;
    expiration_date: string;
    id: number;
    name: string;
    start_date: string;
    status: string;
}