import { FormDate } from './../../models/form-date.model';
import * as moment from "moment";

import {Input, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {StateService} from '@uirouter/core';

import {NgbActiveModal, NgbCalendar} from "@ng-bootstrap/ng-bootstrap";
import {GlWindowRefService} from "../../../gl-common/services/gl-window-ref.service";
import {GlAccountsServise} from "../../services/gl-accounts.servise";


@Component({
    selector: 'gl-create-account-popup',
    styles: [require('./gl-create-account-popup.component.scss').toString()],
    templateUrl: './gl-create-account-popup.component.html'

})
export class GlCreateAccountPopupComponent implements OnInit {
    @Output() resultAccountEvent = new EventEmitter<Omit<Account, 'id'>>();

    statusList: Array<String> = [
        'Disable',
        'Pending',
        'Active'
    ];

    defaultSelect: string = 'Active';
    defaultStartDate: FormDate;
    defaultExpireDate: FormDate;

    constructor(public $state: StateService,
                public activeModal: NgbActiveModal,
                public glWindowRef: GlWindowRefService,
                public glAccountsService: GlAccountsServise,
                public ngbCalendar: NgbCalendar) {}

    ngOnInit() {
        this.defaultStartDate = this.getDefaultDate();
        this.defaultExpireDate = this.getDefaultDate();
        this.defaultExpireDate.year = this.defaultExpireDate.year + 1;
    }

    private getDefaultDate(): FormDate {
        const dt = moment(new Date(), 'YYYY-MM-DD HH:mm:ss');
        return {day: Number(dt.format('DD')), month: Number(dt.format('MM')), year: Number(dt.format('YYYY'))}
    }

    onSubmit(form: NgForm) {
        const account = form.value;
        account.start_date = new Date(`${account.start_date.year}-${account.start_date.month}-${account.start_date.day}`).getTime()/1000;
        account.expiration_date = new Date(`${account.expiration_date.year}-${account.expiration_date.month}-${account.expiration_date.day}`).getTime()/1000;
        this.resultAccountEvent.emit(account);
        this.activeModal.close();
    }

}
