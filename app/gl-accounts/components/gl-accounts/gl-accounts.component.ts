import {Input, Component, EventEmitter} from '@angular/core';
import {StateService} from '@uirouter/core';

import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

import {GlWindowRefService} from "../../../gl-common/services/gl-window-ref.service";
import {GlRootScopeShared} from '../../../glCommon/services/glRootScopeSharedService';

import {GlAccountsServise} from "../../services/gl-accounts.servise";
import {GlCreateAccountPopupComponent} from "../gl-create-account-popup/gl-create-account-popup.component"
import { switchMap } from 'rxjs/operators';

@Component({
    selector: 'gl-accounts',
    styles: [require('./gl-accounts.component.scss').toString()],
    templateUrl: './gl-accounts.component.html'
})
export class GlAccountsComponent {

    accounts: Account[]

    constructor(public $state: StateService,
                public modalService: NgbModal,
                public glWindowRef: GlWindowRefService,
                public glAccountsService: GlAccountsServise,
                public glRootScopeShared: GlRootScopeShared) {
    }

    ngOnInit() {
        this.glAccountsService.getAccounts()
            .subscribe((res: Account[]) => this.accounts = res);
    }


    onRemove(account: Account) {
        this.glAccountsService.deleteAccount(account)
            .subscribe(_=> this.accounts = this.accounts.filter((accountItem: Account) => accountItem.id !== account.id));
    }

    onClickCreateAccount() {
        const modalRef = this.modalService.open(GlCreateAccountPopupComponent);

        modalRef.componentInstance.resultAccountEvent
                .pipe(
                    switchMap((account: Omit<Account, 'id'>) => this.glAccountsService.createAccount({}, account))
                ).subscribe((res: Account) => this.accounts.unshift({...res}));
    }
}
